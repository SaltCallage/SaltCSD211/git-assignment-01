Here's a link to a video showing the assignment being done start to finish:
https://www.youtube.com/watch?v=lYJGrwBY_HQ

**Assignment Instructions**
**You must have GIT bash installed https://gitforwindows.org/**
For the Git for windows installer, make sure the file explorer shell extension is enabled.

**Setting up git bash before first use**
If you have just installed Git, you need to set your identity before you are able to make any commits to your repository. To do this use the following two commands, filling in your name and e-mail where it is required:
```git
git config --global user.name "Your Name"
git config --global user.email "your@email.com"
```
Once you have your identity set in the git config, it is set for all future commits and repositories you interact with on this install of git.


**Step 1: Clone the Repo**
If you installed the File Explorer Shell extension, go into the folder where you want your git-assignment folder to be located in file explorer, right click in the whitespace of the folder (as if to create a new file) and click "Git Bash Here".

Clone the assignment repo and enter the repo directory using:
```git
git clone https://gitlab.com/SaltCSD211/git-assignment-01.git
cd git-assignment-01
```
**Step 2: Create a new branch of the repo with your name**
Now that we have the repository cloned, we're going to make a new branch of the program that you will be able to make changes, commits, and pushes to without affecting the master branch code.

```git
git branch mynamebranch
git checkout mynamebranch
```
Your command line should now say (mynamebranch) instead of (master) at the end. That means that the entire directory that contains your git repository now reflects the directory of the 'mynamebranch'.

**Step 3: Create a directory beside the others with the name of your branch on it**
You have to create an empty text file to give git something to track as well.
```
mkdir mynamebranch
> mynamebranch/submission.txt
```
**Step 4: Stage the new files to be tracked by GIT**
Now that we've simulated making some modifications to the file system let's check the status of our local repository.
```git
git status
```
It should show your new folder in RED... This means that the changes that you made aren't being followed by git and so they won't be pushed and cannot be committed. Let's add the new files to version control by using git add on the folder:
```
git add mynamebranch
```
The proper result of this is for nothing to happen, so check the status again to ensure that your untracked changes have become changes to commit (shown in green).
```
git status
```
Now the changes that you have made are staged to be committed it is time to commit them.

**Step 5: Commit your changes to your local repository.**
Now that we are happy with our changes, we are going to commit them to our local copy of the repository. This should be done often at points where you are happy with your code, you can revert these commits later on should you need to.
```
git commit -m "Initial Commit"
```
The reason "Initial Commit" is written is to indicate that this is the first addition to the repo, normally the message is used to communicate to other developers the changes that you've made with the particular commit.
For this reason, if you're in a multi-developer environment, it might be a wise idea to commit after every describable feature that you develop or change so that those changes are isolated and labeled in your repository.

**Step 6: Verify that git is happy and push changes to the repository**
Let's check on the status of our repo one last time:
```
git status
```
Your message should look like 'Nothing to commit, working branch clean'

**Step 7: Push to the remote!**
This is the go-ahead for pushing our changes to the remote repository. Similar to uploading a file to the internet, everything you've done to your local repository, the creation of a new branch, the new files and the commit will all be sent to the remote repository.

```
git push
```
**Uh Oh!**
Don't worry, you are supposed to get the error!
```
fatal: The current branch mynamebranch has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream origin mynamebranch
```
This is one nice thing about git, the error messages are often helpful!

Enter the command as they provide it and it will allow you to set your new remote branch as upstream.
You will be informed that your new branch is set up to track the remote branch. This should mean that it has pushed successfully. Verify this by checking the status.
```
git status
```
You will see something like this:
```
On branch mynamebranch
Your branch is up to date with 'origin/mynamebranch'.

nothing to commit, working tree clean
```
**Last step: Verify new branch and submit merge request.**
Find your branch in the list located at the following URL:
https://gitlab.com/SaltCSD211/git-assignment-01/branches

Click "Merge Request", scroll to the bottom and click "Submit Merge Request".

You are a developer so you can probably also merge it from here, but just leave that for now!


**Assignment Complete!**
Grats

**Bonus Question**
Add a Java hello world project into your branch folder, commit and push the changes!